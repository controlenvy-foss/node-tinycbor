import { stateRouter } from 'Router'
import './assets/styles/main.scss'

/* Initialize Routes */
import { Home, Docs } from 'Components'
;[Home, Docs].forEach(route => {
  route(stateRouter)
})

stateRouter.evaluateCurrentRoute('Home')
