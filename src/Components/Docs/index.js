import template from './index.html'

export const Docs = stateRouter => {
  stateRouter.addState({
    name: 'Docs',
    route: '/docs',
    template
  })
}
