import template from './index.html'

export const Home = stateRouter => {
  stateRouter.addState({
    name: 'Home',
    route: '/',
    template
  })
}
