/* Routes */
export { Home } from './Home'
export { Docs } from './Docs'

/* Partials */
export { default as Boilerplate } from './_Boilerplate'
export { default as Nav } from './_Nav'
export { default as NavLogo } from './_NavLogo'
export { default as NavLink } from './_NavLink'
export { default as NavButton } from './_NavButton'
export { default as Footer } from './_Footer'
