// Router
import StateRouter from 'abstract-state-router'
import SvelteRenderer from 'svelte-state-renderer'
import HashBrownRouter from 'hash-brown-router'
import SausageRouter from 'sausage-router'

const ENV_SSR = false
let stateRouter = void 0

if (!ENV_SSR) {
  const target = document.querySelector('#app')
  target.innerHTML = ''

  stateRouter = StateRouter(SvelteRenderer(), target, {
    pathPrefix: '',
    router: HashBrownRouter(SausageRouter())
  })
  stateRouter.setMaxListeners(20)

  stateRouter.on('stateChangeEnd', () => {
    window.scrollTo(0, 0)
  })
} else {
  stateRouter = {
    stateIsActive: () => false
  }
}

export { stateRouter }
