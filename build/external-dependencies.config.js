import child from 'child_process'

const dependencies = {
  'assets/highlightjs.css': 'node_modules/highlight.js/styles/atom-one-dark.css'
}

const replacements = {
  'bootstrapper.html': file => {
    child.spawnSync('sed', [
      '-i.original',
      `s/__PREFIX__/edit?file=/g`,
      `./dist/${file}`
    ])
    child.spawnSync('rm', ['-f', `./dist/${file}.original`])
  },
  'bootstrapper-local.html': file => {
    child.spawnSync('sed', ['-i.original', `s/__PREFIX__//g`, `./dist/${file}`])
    child.spawnSync('rm', ['-f', `./dist/${file}.original`])
  }
}

Object.keys(dependencies).forEach(dest => {
  const src = dependencies[dest]
  child.spawnSync('cp', ['-R', `./${src}`, `./dist/${dest}`])
  const replacer = replacements[dest]
  replacer && replacer(dest)
})
