#!/bin/bash
# https://www.gnu.org/software/bash/manual/bashref.html#Pattern-Matching
# To exclude multiple extensions: !(scss|png)
shopt -s extglob

#cp src/assets/*.!(scss) dist/assets
cp -R src/assets/* dist/assets
