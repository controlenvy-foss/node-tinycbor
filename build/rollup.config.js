import inject from 'rollup-plugin-inject'
import resolve from 'rollup-plugin-node-resolve'
import builtins from 'rollup-plugin-node-builtins'
import globals from 'rollup-plugin-node-globals'
import buble from 'rollup-plugin-buble'
import commonjs from 'rollup-plugin-commonjs'
import eslint from 'rollup-plugin-eslint'
import progress from 'rollup-plugin-progress'
import svelte from 'rollup-plugin-svelte'
import scss from 'rollup-plugin-scss'

import highlight from 'highlight.js'

export default {
  entry: 'src/index.js',
  dest: 'dist/index.js',
  format: 'umd',
  moduleName: 'adelyte',
  plugins: [
    progress(),
    commonjs({
      namedExports: {
        svelte: ['compile', 'create', 'parse', 'validate', 'VERSION']
      }
    }),
    resolve({
      extensions: ['.js', '.html'],
      customResolveOptions: {
        moduleDirectory: ['node_modules', 'src']
      }
    }),
    builtins(),
    globals(),
    scss({
      includePaths: ['node_modules', 'src/assets/styles'],
      output: 'dist/assets/styles/main.css'
    }),
    eslint({
      throwError: true,
      exclude: ['node_modules/**']
    }),
    {
      transform(code, id) {
        if (!id.match(/\.html$/)) {
          return code
        }
        const matches = []

        const preCodeRegExp = /<pre.*?>.*?\n.*?<code.*?>(.*\n)*?<\/code>\n.*?<\/pre>/gi

        let match = null
        while ((match = preCodeRegExp.exec(code))) {
          // console.log('match.0', match[0])
          // console.log('match.1', match[1])
          // console.log('match.2', match[2])
          // console.log('match.index', match.index)
          matches.push([match[0], match.index])
        }

        if (matches.length < 1) {
          return code
        }

        const highlightedMatches = matches.map(([text, index]) => {
          let [, beforePreCode, codeToHighlight, afterPreCode] = text.match(
            /(<pre>.*?\n.*?<code>\n)((?:.*\n)*?)(.*?<\/code>.*?\n.*?<\/pre>)/i
          )

          // console.log('codeToHighlight', codeToHighlight)

          let language = void 0
          let result = ''
          try {
            const output = highlight.highlightAuto(codeToHighlight, [
              'javascript'
            ])
            language = output.language
            result = output.value
          } catch (e) {
            console.error(e)
            result = text
            language = false
          }

          // console.log('result', result)

          beforePreCode = beforePreCode.replace(
            /<code>/,
            `<code class="hljs${language ? ' ' + language : ''}">`
          )

          const reconstructed = [beforePreCode, result, afterPreCode].join('')

          return [reconstructed, text.length, index]
        })

        // console.log(highlightedMatches)

        let textDiff = 0
        highlightedMatches.forEach(([highlighted, originalLength, index]) => {
          const idx = index + textDiff
          const before = code.substring(0, idx)
          const after = code.substring(idx + originalLength)

          textDiff += -(originalLength - highlighted.length)

          code = before + highlighted + after
        })

        // if (highlightedMatches.length > 0) {
        //   console.log('code', code)
        // }

        // const highlighted = highlight.highlight(code)
        // console.log(highlighted)
        return code
      }
    },
    svelte({
      dev: process.env.ENV === 'production' ? false : true
    }),
    buble({
      exclude: ['node_modules/**'],
      transforms: {
        dangerousTaggedTemplateString: true
      }
    }),
    inject({
      include: ['src/**'],
      exclude: ['node_modules/**'],
      modules: {
        _: 'lodash'
      }
    })
  ]
}
